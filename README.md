[![pipeline status](https://gitlab.com/niche-devs/destination-west/badges/main/pipeline.svg)](https://gitlab.com/niche-devs/destination-west/-/commits/main)

# Destination West

An adventure western game. Work in progress.

# Play online

You can play Destination West anytime in your browser! Press the button below!
### [`Play`](https://niche-devs.gitlab.io/destination-west/)
