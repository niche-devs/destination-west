/*
  file: src/static/soundMap.js
  purpose: define names of sounds, their audio file and kind
  author: Michał Miłek <https://gitlab.com/mmilek>
  
  This file is part of 'Destination West'.
  
  'Destination West' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License,
  or (at your option) any later version.

  'Destination West' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  'Destination West'. If not, see <https://www.gnu.org/licenses/>.
*/

import { Sound, soundKind } from "./Sound.js";

const soundMap = new Map([
  ['revolver-shot', new Sound('sounds/revolver/revolver-shot.mp3', soundKind.Effect)],
  ['revolver-load', new Sound('sounds/revolver/revolver-load.mp3', soundKind.Effect)],
  ['revolver-roll', new Sound('sounds/revolver/revolver-roll.mp3', soundKind.Effect)],
]);

export default soundMap;
