/*
  file: src/npcNames.js
  purpose: define arrays used in NPC name generation
  author: Oskar Kiliańczyk <https://gitlab.com/oskkil>
  
  This file is part of 'Destination West'.
  
  'Destination West' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License,
  or (at your option) any later version.

  'Destination West' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  'Destination West'. If not, see <https://www.gnu.org/licenses/>.
*/

// arrays holding name and surname to generate full name of the NPC

//name
const maleNames = [
  "Joseph",
  "Leon",
  "Alvin",
  "Harry",
  "Sam",
  "Dave",
  "Jeff",
  "Guy",
  "Elmer",
  "Thomas",
  "Jim",
  "Patrick",
  "Otis",
  "Martin",
  "Christopher",
  "Victor",
  "Stephen",
  "Arnold",
  "Nathan",
  "Jake",
  "Perry",
  "Adam",
  "Arthur",
  "Grover",
  "Lee",
  "Steve",
  "Phillip",
  "Paul",
  "Emil",
  "Jasper",
  "Cornelius",
  "Robert",
  "Blake",
  "Oliver",
  "Levi"
];
const femaleNames = [
  "Catherine",
  "Olive",
  "Adeline",
  "Isabelle",
  "Victoria",
  "Gertrude",
  "Marguerite",
  "Amanda",
  "Susie",
  "Effie",
  "Caroline",
  "Augusta",
  "Jessie",
  "Kate",
  "Dorothy",
  "Julia",
  "Leona",
  "Eliza",
  "Myrtle",
  "Rachel",
  "Dollie",
  "Clara",
  "Mary",
  "Iva",
  "Stella",
  "Edna",
  "Lula",
  "Anna",
  "Ruby",
  "Lucy",
  "Nina"
];

//nickname
const nicknames = [
  "the Kid",
  "Demon Eyes",
  "Serpent",
  "Scarface",
  "the Bullet",
  "Brawn",
  "Killer",
  "Dusty",
  "the Predator",
  "Stab",
  "Long Shot",
  "Faih",
  "the Thinker",
  "Knife",
  "Ace High",
  "Mystery",
  "Immoral",
  "Money",
  "Raven",
  "Bloodlust",
  "Coyote",
  "Hunter",
  "Big Gun",
  "Bulldozer",
  "Deadbeat",
  "Sunrise",
  "Wild Hog",
  "Lone Wolf",
  "Crimson",
  "One Eye",
  "Gravedigger",
  "Quick Gun",
  "Dynamite",
  "Bullseye",
  "Pistol",
  "Lefty",
  "Trapper"
];

//surname
const surnames = [
  "White",
  "Whitney",
  "Wall",
  "Holman",
  "Russell",
  "Carlson",
  "Wolf",
  "Powell",
  "McLean",
  "Mack",
  "Lane",
  "Goff",
  "Glenn",
  "Bray",
  "Cameron",
  "Stanton",
  "Gilmore",
  "Mayer",
  "Goodman",
  "McNeil",
  "Ford",
  "Browning",
  "Coleman",
  "Burris",
  "Elliott",
  "Burnett",
  "Sherman",
  "Jacobs",
  "Blackwell",
  "Hays",
  "Carr",
  "Buckley",
  "Downs",
  "Houston",
  "Green",
  "Lloyd",
  "Marshall",
  "Walker",
  "Roy",
  "Herring",
  "Lang",
  "Spence",
  "Fletcher",
  "Haddock",
  "Hefner",
  "Pearsoll",
  "O'Kones",
  "O'Dane",
  "Coxwell"
];

export { maleNames, femaleNames, nicknames, surnames };
