/*
  file: src/townNames.js
  purpose: define the arrays used in Town name generation
  author: Artur Gulik <https://gitlab.com/ArturGulik>
  
  This file is part of 'Destination West'.
  
  'Destination West' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License,
  or (at your option) any later version.

  'Destination West' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  'Destination West'. If not, see <https://www.gnu.org/licenses/>.
*/

// arrays holding parts of string to generate full name of the Town

const arr1 = [
  "Copper",
  "Wide",
  "Lead",
  "Torn",
  "Abandon",
  "Desert",
  "Fair",
  "Whisper",
  "Dead",
  "False",
  "Lost",
  "Evil",
  "Red",
  "Farm",
  "Windy",
  "Skull",
  "Old",
  "Sand",
  "Forsaken",
  "Wrath",
  "Grave",
  "Low",
  "Wild",
  "Golden",
  "Broken",
  "Silver",
  "Vain",
  "Devil's",
  "Bull's",
  "New",
  "Dark",
  "Rapid",
  "Oat",
  "Tomb",
  "Dodge"
];

const arr2 = [
  "city",
  "wood",
  "stone",
  "field",
  "peaks",
  "river",
  "point",
  "howl",
  "hill",
  "roost",
  "cliff",
  "george",
  "cross",
  "edge",
  "canyon",
  "tooth",
  "town",
  "alley",
  "post",
  "dune",
  "mountain",
  "gate",
  "lake",
  "scar",
  "rock",
  "rise",
  "range",
  "reach",
  "dale",
  "flats",
  "pass",
  "Gate",
  "chapel",
  "hallow",
  "trails",
  "man",
  "ton"
];

export { arr1, arr2 };

